var express = require('express');
var router = express.Router();

let BooksModel = require("../db/models/Books");
let UsersModel = require("../db/models/Users");
let Error = require("../lib/Error");
let Response = require("../lib/Response");
let Check = require("../lib/Check");
let Enum = require("../config/Enum");

/**
 * Get books
 * 
 * @returns {Array} All books with book_id and name fields
 * 
*/
router.get("/", async (req, res, next) => {
  try {

    // throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Bad request", "request is bad");
    let books = await BooksModel.find({}, { book_id: true, name: true, user_id: true, _id: false });
    let resp = new Response().generateResponse(Enum.HTTP_CODES.OK, books)
    res.status(resp.code).json(resp)

  } catch (error) {
    let err = new Response().generateError(error);
    res.status(err.code).json(err);
  }
})

/**
 * Get user
 * 
 * @param {Number} book_id 
 * 
 * @returns {Object} user_id and name fields
 * 
*/
router.get("/:book_id", async (req, res, next) => {
  try {

    let book_id = req.params.book_id
    if (new Check().isEmpty(book_id)) {
      throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Validation Failed!", "book_id field is must be filled")
    }

    book_id = !isNaN(parseInt(book_id)) ? parseInt(book_id) : book_id;

    if (!new Check().isEmpty(book_id) && typeof book_id != "number") {
      throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Validation Failed!", "book_id field is must be a number")
    }

    if (!new Check().isEmpty(book_id) && book_id === 0) {
      throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Validation Failed!", "book_id field cannot be 0")
    }

    // throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Bad request", "request is bad");
    let books = await BooksModel.find({ book_id: book_id }, {});

    if (books.length == 0)
      throw new Error(Enum.HTTP_CODES.NOT_FOUND, "Not Found!", `Not found any book with id: ${book_id}`)
    let resp = new Response().generateResponse(Enum.HTTP_CODES.OK, books)
    res.status(resp.code).json(resp)

  } catch (error) {
    let err = new Response().generateError(error);
    res.status(err.code).json(err);
  }
})


/**
 *  Create a book
 * 
 * @param {Object} name book name
 * 
 * @returns {Object} created book object
 * 
 * */
router.post("/", async (req, res, next) => {
  try {

    // throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Bad request", "request is bad");
    let data = req.body;
    if (new Check().isEmpty(data.name)) {
      throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Validation Failed!", "name field is must be filled")
    }
    let books = await BooksModel.find().sort({ book_id: -1 }).limit(1);
    console.log(books, req.body);

    let new_book_id = 0;
    if (books && books.length > 0) {
      new_book_id = books[0].book_id + 1
    }


    let book = new BooksModel({
      book_id: new_book_id,
      name: data.name,
      created_date: new Date()
    })

    await book.save()
    book = JSON.parse(JSON.stringify(book));
    delete book._id;
    delete book.__v;
    delete book.created_date;

    let resp = new Response().generateResponse(Enum.HTTP_CODES.OK, book)
    res.status(resp.code).json(resp)

  } catch (error) {
    console.log(error)
    let err = new Response().generateError(error);
    res.status(err.code).json(err);
  }

})


module.exports = router;
