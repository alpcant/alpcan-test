
let CustomErr = require("./Error")
let Enum = require("../config/Enum")

let instance = null;

class Response {

  constructor() {
    if (!instance) instance = this;
    return instance;
  }

  generateError(error) {

    if (error instanceof CustomErr) {

      return {
        code: error.code,
        error: {
          message: error.message,
          description: error.description
        }
      }
    }

    return {
      code: Enum.HTTP_CODES.INT_SERVER_ERROR,
      error: {
        message: "Server Error!",
        description: "Server temporarily not available!"
      }
    }
  }

  generateResponse(code, data = []) {
    return {
      code: code,
      data: data
    }
  }

}

module.exports = Response;