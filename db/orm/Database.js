let conf = require("../../config");

let instance = null;
class Database {

    constructor() {
        this.db = require("./" + conf.DB_TYPE)
        if (!instance) instance = this;
        return instance;
    }

    async connect() {

        return this.db.connect();


    }
}

module.exports = Database;