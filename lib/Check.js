
const Enum = require("../config/Enum")

let instance = null;

class Check {

  constructor() {
    if (!instance) instance = this;
    return instance;
  }
  /**
   * Check field is emptpy
    */
  isEmpty(data) {
    if (data === "" || data == null || data == undefined)
      return true;
    return false;
  }

  /**
   * Check multiple fields
   */
  areThereEmptyFields(...args) {
    let emptyFields = [...args].filter(x => this.isEmpty(x));

    if (emptyFields.length > 0) {
      throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Validation Failed", `${emptyFields.join(", ")} fields are empty`)
    }
  }


}

module.exports = Check;