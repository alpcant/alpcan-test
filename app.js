var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var Database = require('./db/orm/Database');
var conf = require('./config/index');
var Enum = require('./config/Enum');

var app = express();

// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');

app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));




console.log(conf.DB_TYPE, "connection establishing...")
new Database().connect().then(res => {

  console.log(conf.DB_TYPE, "connected!")


  app.use('/users', require('./routes/users'));
  app.use('/books', require('./routes/books'));
  // app.use('/', indexRouter);


  // app.use('/users', usersRouter);

  // catch 404 and forward to error handler
  app.use(function (req, res, next) {
    next(createError(404));
  });

  // error handler
  app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    // res.status(err.status || 500);
    res.status(Enum.HTTP_CODES.NOT_FOUND).json({ code: Enum.HTTP_CODES.NOT_FOUND, error: { message: "Not Found!", description: "Page not found" } });
  });

}).catch(err => {
  console.error(err.message)

})

module.exports = app;
