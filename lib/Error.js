class CustomErr extends Error {
  constructor(code, message, description) {
    super(`code: ${code}, title: ${message}, description: ${description}`)
    // this.message = `code: ${code}, title: ${title}, description: ${description}`;
    this.code = code;
    this.message = message;
    this.description = description;
    // Error.captureStackTrace(this, CustomErr);
  }
}

module.exports = CustomErr;