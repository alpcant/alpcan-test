var mongoose = require('mongoose');
const Check = require("../../lib/Check");
const Error = require("../../lib/Error");
const Enum = require("../../config/Enum");


const schema = new mongoose.Schema({
    user_id: { type: Number },
    name: { type: String, unique: true },
    books: { type: mongoose.Schema.Types.Mixed/* , ref: 'Books' */ }
});

class User extends mongoose.Model {
    async save() {

        try {

            if (new Check().isEmpty(this.name))
                throw new Error(Enum.HTTP_CODES.UNPROCESSABLE_ENTITY, "Validation Failed", "name field is required");

            return await super.save();

        } catch (error) {
            if (error && error.code == 11000) {
                return Promise.reject(new Error(Enum.HTTP_CODES.FORBIDDEN, "Already Exists", "This user is already Exists"));

            }
            return Promise.reject(error);
        }

    }
}

schema.loadClass(User);
module.exports = mongoose.model('Users', schema);