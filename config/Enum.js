module.exports = {
    HTTP_CODES: {
        OK: 200,
        INT_SERVER_ERROR: 500,
        BAD_REQUEST: 400,
        FORBIDDEN: 403,
        NOT_FOUND: 404,
        UNPROCESSABLE_ENTITY: 422
    }
}