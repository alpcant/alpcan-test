var mongoose = require('mongoose');
const Check = require("../../lib/Check");
const Error = require("../../lib/Error");
const Enum = require("../../config/Enum");


const schema = new mongoose.Schema({
  book_id: Number,
  name: { type: String, unique: true },
  created_date: Date,
  user_id: { type: Number, default: 0 }
});

class Books extends mongoose.Model {

  async save() {

    try {

      if (new Check().isEmpty(this.name))
        throw new Error(Enum.HTTP_CODES.UNPROCESSABLE_ENTITY, "Validation Failed", "name field is required");

      return await super.save();

    } catch (error) {
      if (error && error.code == 11000) {
        return Promise.reject(new Error(Enum.HTTP_CODES.FORBIDDEN, "Already Exists", "This book is already Exists"));

      }
      return Promise.reject(error);
    }

  }

}

schema.loadClass(Books);

module.exports = mongoose.model('Books', schema);