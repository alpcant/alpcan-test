var express = require('express');
var router = express.Router();

let BooksModel = require("../db/models/Books");
let UsersModel = require("../db/models/Users");
let Error = require("../lib/Error");
let Response = require("../lib/Response");
let Check = require("../lib/Check");
let Enum = require("../config/Enum");
const async = require("async");

/**
 * Get users
 * 
 * @returns {Array} All users with user_id and name fields
 * 
*/
router.get('/', async function (req, res, next) {
  try {

    // throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Bad request", "request is bad");
    let users = await UsersModel.find({}, { name: true, user_id: true, _id: false }).lean();

    await new Promise((resolve, reject) => {


      async.eachSeries(users, (user, nextUser) => {

        (async () => {

          try {

            let books = await BooksModel.find({ user_id: user.user_id });
            books = books.map(x => x.name)
            user.books = books;

          } catch (error) {
            console.log(error)
          }

          async.setImmediate(nextUser);

        })()

      }, err => {
        console.log(err)
        return resolve()
      })

    })
    let resp = new Response().generateResponse(Enum.HTTP_CODES.OK, users)
    res.status(resp.code).json(resp)

  } catch (error) {
    let err = new Response().generateError(error);
    res.status(err.code).json(err);
  }
});


/**
 * Get user
 * 
 * @param {Number} user_id 
 * 
 * @returns {Object} User with user_id and name fields
 * 
*/
router.get("/:user_id", async (req, res, next) => {
  try {

    let user_id = req.params.user_id;
    if (new Check().isEmpty(user_id)) {
      throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Validation Failed!", "user_id field is must be filled")
    }

    user_id = !isNaN(parseInt(user_id)) ? parseInt(user_id) : user_id
    if (!new Check().isEmpty(user_id) && typeof user_id != "number") {
      throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Validation Failed!", "user_id field is must be a number")
    }

    if (!new Check().isEmpty(user_id) && user_id === 0) {
      throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Validation Failed!", "user_id field cannot be 0")
    }

    // throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Bad request", "request is bad");
    let user = await UsersModel.findOne({ user_id: user_id }, { name: true, user_id: true, _id: false }).lean();

    if (!user)
      throw new Error(Enum.HTTP_CODES.NOT_FOUND, "Not Found!", `Not found any user with user_id: ${user_id}`)

    let usersBooks = await BooksModel.find({ user_id: user_id }).lean();

    user.books = usersBooks.map(x => x.name);

    let resp = new Response().generateResponse(Enum.HTTP_CODES.OK, user)
    res.status(resp.code).json(resp)

  } catch (error) {
    let err = new Response().generateError(error);
    res.status(err.code).json(err);
  }
})


/**
 *  Generate a user
 * 
 * @param {Object} name user name
 * 
 * @returns {Object} generated user object
 * 
 * */
router.post("/", async (req, res, next) => {
  try {

    // throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Bad request", "request is bad");
    let data = req.body;
    if (new Check().isEmpty(data.name)) {
      throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Validation Failed!", "name field is must be filled")
    }
    let user = await UsersModel.findOne().sort({ user_id: -1 });

    let new_user_id = 1;
    if (user) {
      new_user_id = user.user_id + 1
    }


    let userBag = new UsersModel({
      books: [],
      name: data.name,
      user_id: new_user_id
    })

    await userBag.save()
    userBag = JSON.parse(JSON.stringify(userBag))
    delete userBag._id;
    delete userBag.__v;
    delete userBag.created_date;

    let resp = new Response().generateResponse(Enum.HTTP_CODES.OK, userBag)
    res.status(resp.code).json(resp)

  } catch (error) {
    console.log(error)
    let err = new Response().generateError(error);
    res.status(err.code).json(err);
  }

})

/**
 * Borrow a book
 * 
 * @param {Object} user_id 
 * @param {Object} book_id 
 * 
 * @returns {Message} success or fail message
 **/
router.post("/:user_id/borrow/:book_id", async (req, res, next) => {

  try {

    let { user_id, book_id } = req.params;

    new Check().areThereEmptyFields(user_id, book_id)

    book_id = !isNaN(parseInt(book_id)) ? parseInt(book_id) : book_id;

    if (!new Check().isEmpty(book_id) && typeof book_id != "number") {
      throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Validation Failed!", "book_id field is must be a number")
    }

    user_id = !isNaN(parseInt(user_id)) ? parseInt(user_id) : user_id;

    if (!new Check().isEmpty(user_id) && typeof user_id != "number") {
      throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Validation Failed!", "user_id field is must be a number")
    }

    if (!new Check().isEmpty(user_id) && user_id === 0) {
      throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Validation Failed!", "user_id field cannot be 0")
    }


    let selectedBook = await BooksModel.find({ book_id });
    if (selectedBook.length == 0)
      throw new Error(Enum.HTTP_CODES.UNPROCESSABLE_ENTITY, "Validation Failed!", "Book not found!");

    let selectedUser = await UsersModel.find({ user_id })
    if (selectedUser.length == 0)
      throw new Error(Enum.HTTP_CODES.UNPROCESSABLE_ENTITY, "Validation Failed!", "User not found!");

    selectedBook = selectedBook[0];


    //Exceptional situations
    if (selectedBook.user_id > 0) {
      if (selectedBook.user_id == user_id) {
        throw new Error(Enum.HTTP_CODES.UNPROCESSABLE_ENTITY, "Validation Failed!", `You already borrowed '${selectedBook.name}'`);
      }
      else {
        throw new Error(Enum.HTTP_CODES.UNPROCESSABLE_ENTITY, "Validation Failed!", "This book took by another people");
      }
    }


    await BooksModel.update({ book_id: book_id }, { user_id: user_id });


    let resp = new Response().generateResponse(Enum.HTTP_CODES.OK, { message: "Success", description: `'${selectedBook.name}' successfully borrowed` })
    res.status(resp.code).json(resp)

  } catch (error) {
    console.log(error)
    let err = new Response().generateError(error);
    res.status(err.code).json(err);
  }



})

/**
 * return a book
 * 
 * @param {Object} user_id 
 * @param {Object} book_id 
 * 
 * @returns {Message} success or fail message
 **/
router.post("/:user_id/return/:book_id", async (req, res, next) => {

  try {

    let { user_id, book_id } = req.params;

    new Check().areThereEmptyFields(user_id, book_id)

    book_id = !isNaN(parseInt(book_id)) ? parseInt(book_id) : book_id;

    if (!new Check().isEmpty(book_id) && typeof book_id != "number") {
      throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Validation Failed!", "book_id field is must be a number")
    }


    user_id = !isNaN(parseInt(user_id)) ? parseInt(user_id) : user_id;

    if (!new Check().isEmpty(user_id) && typeof user_id != "number") {
      throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Validation Failed!", "user_id field is must be a number")
    }

    if (!new Check().isEmpty(user_id) && user_id === 0) {
      throw new Error(Enum.HTTP_CODES.BAD_REQUEST, "Validation Failed!", "user_id field cannot be 0")
    }


    let selectedBook = await BooksModel.find({ book_id });
    if (selectedBook.length == 0)
      throw new Error(Enum.HTTP_CODES.UNPROCESSABLE_ENTITY, "Validation Failed!", "Book not found!");

    let selectedUser = await UsersModel.find({ user_id })
    if (selectedUser.length == 0)
      throw new Error(Enum.HTTP_CODES.UNPROCESSABLE_ENTITY, "Validation Failed!", "User not found!");

    selectedBook = selectedBook[0];

    //Exceptional situations
    if (selectedBook.user_id > 0) {
      if (selectedBook.user_id != user_id) {
        // throw new Error(Enum.HTTP_CODES.UNPROCESSABLE_ENTITY, "Validation Failed!", "You didn't borrowed this book");
        throw new Error(Enum.HTTP_CODES.UNPROCESSABLE_ENTITY, "Validation Failed!", "This book took by another people, so you can not borrow it");
      }
      else {
        // success
      }
    }
    else {
      throw new Error(Enum.HTTP_CODES.UNPROCESSABLE_ENTITY, "Validation Failed!", "You didn't borrowed this book");
    }


    await BooksModel.update({ book_id: book_id }, { user_id: 0 });


    let resp = new Response().generateResponse(Enum.HTTP_CODES.OK, { message: "Success", description: `'${selectedBook.name}' successfully returned` })
    res.status(resp.code).json(resp)

  } catch (error) {
    console.log(error)
    let err = new Response().generateError(error);
    res.status(err.code).json(err);
  }



})

module.exports = router;
