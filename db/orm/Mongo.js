const mongoose = require("mongoose");
const conf = require("../../config/index");


let instance = null;
class Mongo {

    constructor() {
        if (!instance) instance = this;
        return instance;
    }

    async connect() {

        return new Promise((resolve, reject) => {
            mongoose.connect('mongodb://localhost:27017/' + conf[conf.DB_TYPE].DB_NAME, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useCreateIndex: true,
                serverSelectionTimeoutMS: 3000
            }).then(res => {
                resolve(res)
            }).catch(error => {
                reject(error);
            });
        })
    }
}

module.exports = new Mongo();